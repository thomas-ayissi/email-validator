# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.0] - 2021-11-01

### Added

- A ZeroBounce API key can be specified and that service will be used to perform SMTP validation.

## [1.3.0] - 2021-09-18

### Changed

- Handled new `TLSNegotiationError` raised by `py3-validate-email`.

## [1.2.1] - 2021-09-12

### Added

- Added an endpoint to retrieve web service logs (`/logs`) as a ZIP file.

## [1.2.0] - 2021-09-05

### Added

- Exposed settings to control the validation process.

### Changed

- Improved how logs are sent to Azure Application Insights.

## [1.1.3] - 2021-09-04

### Added

- Added missing logs to validation process.

### Changed

- Improved how logs are sent to Azure Application Insights.

## [1.1.2] - 2021-09-03

### Added

- Logs are optionally sent to Azure Application Insights.
- `APPINSIGHTS_INSTRUMENTATIONKEY` configuration can be used to specify
  Azure Application Insights Instrumentation Key.

## [1.1.1] - 2021-08-28

### Changed

- `HEALTH_CHECK_PROBE_WEBSITES` configuration has been renamed to `HEALTH_CHECK_PING_WEBSITES`.
- `HEALTH_CHECK_PROBE_TIMEOUT` configuration has been renamed to `HEALTH_CHECK_PING_TIMEOUT`.

## [1.1.0] - 2021-08-28

### Added

- `HEALTH_CHECK_PROBE_WEBSITES` configuration has been added to replace `HEALTH_CHECK_PING_IP_ADDRESSES`.
  Google and DuckDuckGo are probed by default.
- `HEALTH_CHECK_PROBE_TIMEOUT` configuration has been added. Default value is one second.

### Removed

- `HEALTH_CHECK_PING_IP_ADDRESSES` configuration has been removed.
  ICMP packets are not supported on many infrastructures, such as Azure App Service.

## [1.0.0] - 2021-08-28

### Added

- Initial release.

[2.0.0]: https://gitlab.com/pommalabs/email-validator/-/compare/1.3.0...2.0.0
[1.3.0]: https://gitlab.com/pommalabs/email-validator/-/compare/1.2.1...1.3.0
[1.2.1]: https://gitlab.com/pommalabs/email-validator/-/compare/1.2.0...1.2.1
[1.2.0]: https://gitlab.com/pommalabs/email-validator/-/compare/1.1.3...1.2.0
[1.1.3]: https://gitlab.com/pommalabs/email-validator/-/compare/1.1.2...1.1.3
[1.1.2]: https://gitlab.com/pommalabs/email-validator/-/compare/1.1.1...1.1.2
[1.1.1]: https://gitlab.com/pommalabs/email-validator/-/compare/1.1.0...1.1.1
[1.1.0]: https://gitlab.com/pommalabs/email-validator/-/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/pommalabs/email-validator/-/tags/1.0.0
