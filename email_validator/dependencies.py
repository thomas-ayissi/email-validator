# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import functools
import hmac
import logging
import os
import subprocess
from datetime import datetime, timezone
from functools import lru_cache
from logging.handlers import TimedRotatingFileHandler

from fastapi import Security, status
from fastapi.exceptions import HTTPException
from fastapi.param_functions import Depends
from fastapi.security.api_key import APIKeyHeader
from healthcheck import HealthCheck
from opencensus.ext.azure.log_exporter import AzureLogHandler
from opencensus.ext.azure.trace_exporter import AzureExporter
from opencensus.trace.samplers import AlwaysOffSampler, AlwaysOnSampler
from opencensus.trace.tracer import Tracer
from zerobouncesdk import zerobouncesdk

from email_validator.settings import (
    AppInsightsSettings,
    HealthCheckSettings,
    SecuritySettings,
    ValidatorSettings,
    ZeroBounceSettings,
)

API_KEY_HEADER_NAME = "X-Api-Key"
LOGS_PATH = "./logs"

api_key_header_auth = APIKeyHeader(name=API_KEY_HEADER_NAME, auto_error=False)


def __get_validator_settings():
    return ValidatorSettings()


@lru_cache()
def get_validator_settings():
    return __get_validator_settings()


def __get_appinsights_settings():
    return AppInsightsSettings()


@lru_cache()
def get_appinsights_settings():
    return __get_appinsights_settings()


def __get_zerobounce_settings():
    return ZeroBounceSettings()


@lru_cache()
def get_zerobounce_settings():
    return __get_zerobounce_settings()


def __get_logger(appinsights_settings: AppInsightsSettings):
    logger = logging.getLogger(__name__)

    if len(logger.handlers) > 0:
        # Logger has already been configured.
        return logger

    # Copy log level from uvicorn logger, which is controlled via command line arguments.
    uvicorn_logger = logging.getLogger("uvicorn")
    logger.setLevel(uvicorn_logger.level)

    # Use handlers of uvicorn logger, since they print beautiful messages to the console.
    # Moreover, by using the same list, additional handlers will be shared
    # (like, for example, the one for Azure Application Insights).
    logger.handlers = uvicorn_logger.handlers

    file_handler = TimedRotatingFileHandler(
        os.path.join(LOGS_PATH, "app.log"), when="d", backupCount=31, utc=True
    )
    file_handler.setFormatter(
        logging.Formatter("%(asctime)s [%(levelname)s] %(message)s")
    )
    logger.addHandler(file_handler)

    if appinsights_settings.instrumentation_key:
        logger.addHandler(
            AzureLogHandler(connection_string=appinsights_settings.connection_string)
        )

    return logger


@lru_cache()
def get_logger(
    appinsights_settings: AppInsightsSettings = Depends(get_appinsights_settings),
):
    return __get_logger(appinsights_settings)


def __get_tracer(appinsights_settings: AppInsightsSettings):
    if appinsights_settings.instrumentation_key:
        return Tracer(
            exporter=AzureExporter(
                connection_string=appinsights_settings.connection_string
            ),
            sampler=AlwaysOnSampler(),
        )

    return Tracer(
        exporter=None,
        sampler=AlwaysOffSampler(),
    )


def get_tracer(
    appinsights_settings: AppInsightsSettings = Depends(get_appinsights_settings),
):
    return __get_tracer(appinsights_settings)


def __get_security_settings():
    return SecuritySettings()


@lru_cache()
def get_security_settings():
    return __get_security_settings()


async def get_api_key(
    api_key_header: str = Security(api_key_header_auth),
    logger: logging.Logger = Depends(get_logger),
    security_settings: SecuritySettings = Depends(get_security_settings),
):
    if not security_settings.enabled:
        # Security has been disabled; therefore, no check on API keys has to be performed.
        return

    if not api_key_header:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Missing API key"
        )

    # Step 1: look for an API key with received value.
    matched_api_keys = filter(
        # When validating an API key, timing attacks can be mitigated
        # by avoiding to compare strings with the "==" operator.
        lambda ak: hmac.compare_digest(ak.value, api_key_header),
        security_settings.api_keys,
    )

    # Step 2: make sure that filtered API keys have not expired.
    matched_api_keys = filter(
        lambda ak: ak.expires_at is None or ak.expires_at >= datetime.now(timezone.utc),
        matched_api_keys,
    )

    matched_api_keys = list(matched_api_keys)

    if not matched_api_keys:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid API key",
        )

    matched_api_key = matched_api_keys[0]
    logger.info('Validated an API key with name "%s"', matched_api_key.name)

    return matched_api_key


def __get_health_check_settings():
    return HealthCheckSettings()


@lru_cache()
def get_health_check_settings():
    return __get_health_check_settings()


def __ping_website(
    website: str,
    logger: logging.Logger,
    health_check_settings: HealthCheckSettings,
):
    """Returns True if specified website responds to an HTTP HEAD request."""

    command = [
        "curl",
        "--fail",
        "--head",
        "--max-time",
        str(health_check_settings.ping_timeout),
        "--output",
        "/dev/null",
        "--retry-connrefused",
        "--retry",
        "3",
        "--show-error",
        "--silent",
        website,
    ]

    try:
        subprocess.check_output(command)
        return True, "Ping is OK"
    except subprocess.CalledProcessError as cpe:
        logger.warning(
            'wget command for website "%s" exited with code %s. Command output: %s',
            website,
            cpe.returncode,
            cpe.output,
            exc_info=True,
        )
        return False, "Ping is KO"


def __check_zerobounce_credits(
    logger: logging.Logger, zerobounce_settings: ZeroBounceSettings
):
    zerobounce_response = zerobouncesdk.get_credits()
    available_credits = int(zerobounce_response.credits)
    credits_threshold = zerobounce_settings.credits_threshold

    if available_credits >= credits_threshold:
        return True, "Credits check is OK"

    logger.warning(
        "Available ZeroBounce credits (%s) are below the configured threshold (%s)",
        available_credits,
        credits_threshold,
    )
    return False, f"Credits check is KO ({available_credits}/{credits_threshold})"


def __get_health_check(
    logger: logging.Logger,
    health_check_settings: HealthCheckSettings,
    zerobounce_settings: ZeroBounceSettings,
):
    health_check = HealthCheck(failed_status=503)

    for i, ip_address in enumerate(health_check_settings.ping_websites, start=1):
        check = functools.partial(
            __ping_website, ip_address, logger, health_check_settings
        )
        setattr(check, "__name__", f"ping_website_{i}")
        health_check.add_check(check)

    if zerobounce_settings.api_key:
        check = functools.partial(
            __check_zerobounce_credits, logger, zerobounce_settings
        )
        setattr(check, "__name__", "check_zerobounce_credits")
        health_check.add_check(check)

    return health_check


@lru_cache()
def get_health_check(
    logger: logging.Logger = Depends(get_logger),
    health_check_settings: HealthCheckSettings = Depends(get_health_check_settings),
    zerobounce_settings: ZeroBounceSettings = Depends(get_zerobounce_settings),
):
    return __get_health_check(logger, health_check_settings, zerobounce_settings)
