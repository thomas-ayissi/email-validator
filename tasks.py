import os

from invoke import task

PROJECT_MODULE = "email_validator"
DEFAULT_PORT = "8080"
RUN_ARGS = {"pty": True}


@task
def git_version(ctx):
    version_number = (
        ctx.run("git describe --tags", **RUN_ARGS).stdout.strip().split("-")[0]
    )
    commit_sha = ctx.run("git rev-parse --short HEAD", **RUN_ARGS).stdout.strip()
    version = f"{version_number}+{commit_sha}"
    ctx.run(f"poetry version {version}", **RUN_ARGS)


@task
def run_tests(ctx):
    coverage_flags = f"--cov-report xml:coverage.xml --cov={PROJECT_MODULE} --cov tests"
    ctx.run(f"pytest --junitxml=test-report.xml {coverage_flags} tests/", **RUN_ARGS)


@task
def start_dev_server(ctx):
    port = os.getenv("PORT", DEFAULT_PORT)
    uvicorn_flags = (
        f"--port {port} --no-server-header --reload --use-colors --log-level debug"
    )
    ctx.run(f"uvicorn {PROJECT_MODULE}.main:app {uvicorn_flags}", **RUN_ARGS)


@task
def start_server(ctx):
    port = os.getenv("PORT", DEFAULT_PORT)
    uvicorn_flags = (
        f"--host 0.0.0.0 --port {port}"
        " --no-access-log --no-server-header --no-use-colors"
    )
    ctx.run(f"uvicorn {PROJECT_MODULE}.main:app {uvicorn_flags}", **RUN_ARGS)
