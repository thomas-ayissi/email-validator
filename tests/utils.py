# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging

from email_validator import dependencies, validator
from validate_email.email_address import EmailAddress
from validate_email.exceptions import AddressFormatError


STUB_ZEROBOUNCE_API_KEY = "API_KEY"


class AnyArg:
    def __eq__(self, _):
        return True


def clear_caches():
    root_logger = logging.getLogger()
    root_logger.manager.loggerDict.clear()

    dependencies.get_appinsights_settings.cache_clear()
    dependencies.get_health_check.cache_clear()
    dependencies.get_health_check_settings.cache_clear()
    dependencies.get_logger.cache_clear()
    dependencies.get_security_settings.cache_clear()
    dependencies.get_validator_settings.cache_clear()
    dependencies.get_zerobounce_settings.cache_clear()

    validator.validate.cache_clear()


def get_stub_logger():
    return logging.getLogger(__name__)


def is_parsable_address(address: str) -> bool:
    try:
        EmailAddress(address)
        return True
    except AddressFormatError:
        return False
